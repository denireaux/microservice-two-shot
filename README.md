# Wardrobify

Team:

* D'Angelo - Worked on hats microservice
* Yadriel - Worked on shoes microservice

## Design
![Alt text](image-1.png)

## Shoes microservice
Created models for Shoes and the integration of the bin from wardrobe.
Using the Shoe model the user can add manufacturer, the model name of the shoe, color, add a picture link and choose in what bin of the wardrobe it is in.
With this integration of the microservices the user can maintain a list of their shoes, as well as deleting a shoe from the list and adding a shoe to the list.


## Hats microservice
Created hat model as well as integration of the location model from wardrobe. The end user is able to add fabric, the name of the style, color, a URL for a picture, and the location the hat is, in the warddrobs. Given the user has hats saved in the database, they are able to keep track of where all of their specific hats are, andd which one's are where. From the hats list page, users may also delete a specific hat if they so choose. 