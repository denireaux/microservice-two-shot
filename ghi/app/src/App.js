import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesForm from './ShoesForm';
import ShoeList from './ShoeList';
import HatsForm from './HatsForm';
import HatList from './HatList';

function App(props) {

  if (props.hats === undefined) {
    return null;
  }

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes/" element={<ShoeList shoes={props.shoes} />} />
          <Route path="/shoes/create/" element={<ShoesForm />} />
          <Route path="/hats" element={<HatList hats={props.hats} />} />
          <Route path="/hats/create" element={<HatsForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;