import React, { useState, useEffect } from "react";


function HatsForm() {

    //drop down list for locations
    const [locations, setLocations] = useState([])
    const [fabric, setFabric] = useState('')
    const [styleName, setStyleName] = useState('')
    const [color, setColor] = useState('')
    const [pictureUrl, setPictureUrl] = useState('')
    const [location, setLocation] = useState('')

    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }

    const handleStyleNameChange = (event) => {
        const value = event.target.value;
        setStyleName(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {}
        data.fabric = fabric;
        data.style_name = styleName;
        data.color = color;
        data.picture_url = pictureUrl;
        data.location = location;
        const hatsUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method : 'POST',
            body : JSON.stringify(data),
            headers : {'Content-Type' : 'application/json'}
        }
        const hatResponse = await fetch(hatsUrl, fetchConfig);
        if (hatResponse.ok) {
            const newHat = await hatResponse.json();
            console.log(newHat)
            setFabric('');
            setStyleName('');
            setColor('');
            setPictureUrl('');
            setLocation('');
        }
    }


    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/'
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
            console.log(data.locations)
        }
    }

    useEffect(() => {
        fetchData();
    }, []); 


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new Hat</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input value={fabric} onChange={handleFabricChange} placeholder="Fabric" name="name" required type="text" id="fabric" className="form-control" />
                            <label htmlFor="name">Fabric</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={styleName} onChange={handleStyleNameChange} placeholder="Style Name" name="starts" required type="text" id="styleName" className="form-control" />
                            <label htmlFor="starts">Style Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={color} onChange={handleColorChange} placeholder="Color" name="ends" required type="text" id="color" className="form-control" />
                            <label htmlFor="ends">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={pictureUrl} onChange={handlePictureUrlChange} placeholder="URL" name="ends" type="url" id="pictureUrl" className="form-control" />
                            <label htmlFor="ends">URL For a Picture</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select value={location} onChange={handleLocationChange} required id="location" className="form-select" name="location">
                            <option value={''}>Choose a Location</option>
                            {locations.map(location => {
                                    return (
                                        <option key={location.href} value={location.href}>
                                            {location.closet_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default HatsForm;