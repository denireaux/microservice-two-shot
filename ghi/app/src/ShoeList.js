function ShoeList(props) {

    const handleDelete = async (event) => {
        const id = event.target.parentElement.parentElement.id;
        const url = `http://localhost:8080/api/shoes/${id}/`;
        const fetchConfig = {
            method: 'DELETE',
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            window.location.reload();
        }
    }

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model</th>
                    <th>Color</th>
                    <th>Bin</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
                {props.shoes.map(shoe => {
                    return (
                        <tr key={shoe.id} id={shoe.id}>
                            <td>{shoe.manufacturer}</td>
                            <td>{shoe.model_name}</td>
                            <td>{shoe.color}</td>
                            <td>{shoe.bin}</td>
                            <td><img src={shoe.picture} alt={shoe.model_name} width={210} height={200} /></td>
                            <td><button onClick={handleDelete} id='delete-button'>Delete</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default ShoeList;