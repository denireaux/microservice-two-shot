import React, { useState, useEffect } from 'react';

function ShoesForm() {
    const [bins, setBins] = useState([]);

    const [manufacturer, setManufacturer] = useState("");
    const handleManufacturerChange = (event) => {
        setManufacturer(event.target.value);
    }

    const [modelName, setModelName] = useState("");
    const handleModelNameChange = (event) => {
        setModelName(event.target.value);
    }

    const [color, setColor] = useState("");
    const handleColorChange = (event) => {
        setColor(event.target.value);
    }

    const [picture, setPicture] = useState("");
    const handlePictureChange = (event) => {
        setPicture(event.target.value);
    }

    const [bin, setBin] = useState("");
    const handleBinChange = (event) => {
        setBin(event.target.value);
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);

        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.manufacturer = manufacturer;
        data.model_name = modelName;
        data.color = color;
        data.picture = picture;
        data.bin = bin;

        const url = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newShow = await response.json();
            console.log(newShow);

            setBin("");
            setColor("");
            setManufacturer("");
            setModelName("");
            setPicture("");

        };
        const formTag = document.getElementById('create-shoe-form');
        formTag.reset();
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new shoe</h1>
                    <form onSubmit={handleSubmit} id="create-shoe-form">

                        <div className="form-floating mb-3">
                            <input onChange={handleManufacturerChange} placeholder="Manufacturer" required type="text" id="manufacturer" name="manufacturer" className="form-control" />
                            <label htmlFor="manufacturer">Manufacturer</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input onChange={handleModelNameChange} type="text" className="form-control" id="name" name="name" />
                            <label htmlFor="name">Model Name</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} type="text" className="form-control" id="color" name="color" />
                            <label htmlFor="color">Color</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input onChange={handlePictureChange} className="form-control" type="url" id="picture" name="picture" />
                            <label htmlFor="picture">Link a picture</label>
                        </div>

                        <div className="mb-3">
                            <select onChange={handleBinChange} required id="bin" className="form-select" name="bin">
                                <option value="">Choose a bin</option>
                                {bins.map(bin => {
                                    return (
                                        <option key={bin.href} value={bin.href}>{bin.closet_name}</option>
                                    );
                                })}
                            </select>
                        </div>

                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default ShoesForm;