from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import Hat, LocationVO
import json


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = ["import_href", "closet_name"]


class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {"location": LocationVOEncoder()}

    def get_extra_data(self, o):
        return {"location" : o.location.id}
    

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "id",
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]

    encoders = {"location": LocationVOEncoder()}


#retieve list of hats/create a new hat
@require_http_methods(["GET", "POST"])
def api_list_hats(request):

    #for getting the list of hats
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
            safe=False
        )
    
    #for creating a new hat
    else:
        content = json.loads(request.body)

        #does the provided href match an existing?
        try:
            location = LocationVO.objects.get(import_href=content["location"])
            content["location"] = location

        #href does not match an existing
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message" : "Check Location"},
                status=400,
                )
        
        #create the new hat
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatListEncoder,
            safe=False)


#Get hat detail
@require_http_methods(["GET", "DELETE", "PUT"])
def api_hat_detail(request, pk):

    #show hat details
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder=HatDetailEncoder,
                safe=False
            )
        
        except:
            return JsonResponse({"error" : "Selected hat does not exist."})
        

    #delete a hat
    elif request.method == "DELETE":
        try:
            hat = Hat.objects.get(id=pk)
            count, _ = Hat.objects.get(id=pk).delete()
            return JsonResponse({"deleted" : count > 0})
        except:
            return JsonResponse({"error" : "Selected hat does not exist."})
        

    #update a hat
    else:
        content = json.loads(request.body)

        #handle if a location doesn't exist
        try:
            if "location" in content:
                location = LocationVO.objects.get(id=content["location"])
                content["location"] = location 
        except:
            return JsonResponse({"message" : "Selected location does not exist"})
        
        #handle if a hat doesn't exist
        try:
            Hat.objects.filter(id=pk).update(**content)
            hat = Hat.objects.get(id=pk)
        except:
            return JsonResponse({"message" : "Selected hat does not exist"})
        
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False
        )