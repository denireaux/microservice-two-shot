from django.urls import path
from .api_views import show_all_shoes, detail_shoes

urlpatterns = [
    path('shoes/<int:id>/', detail_shoes, name='detail_shoes'),
    path('shoes/', show_all_shoes, name='show_all_shoes'),
]