from django.http import JsonResponse
import json
from common.json import ModelEncoder
from .models import Shoes, BinVO
from django.views.decorators.http import require_http_methods

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "import_href",
        "id"
    ]

class ShoeDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture",
        "bin",
        'id'
    ]
    encoders = {
        "bin": BinVOEncoder()
    }

class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture",
        "bin",
        'id'
    ]
    encoders = {
        "bin": BinVOEncoder()
    }
    
    def get_extra_data(self, o):
        return {
            "bin":o.bin.id
        }
    

@require_http_methods(["GET", "POST"])
def show_all_shoes(request):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesListEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            bin = BinVO.objects.get(import_href=content["bin"])
            content["bin"] = bin
            
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"error": "Bin does not exist"},
                status=400
            )
            
        shoes = Shoes.objects.create(**content)
        return JsonResponse(    
            {"shoes": shoes},
            encoder=ShoesListEncoder,
            safe=False
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def detail_shoes(request, id):
    try:
        shoes = Shoes.objects.get(id=id)
    except Shoes.DoesNotExist:
        return JsonResponse(
            {"error": "Shoes does not exist"},
            status=400
        )
    
    if request.method == "GET":
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        shoes.delete()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeDetailEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        try:
            bin = BinVO.objects.get(import_href=content["bin"])
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"error": "Bin does not exist"},
                status=400
                )
        Shoes.objects.filter(id=id).update(**content)
        shoes = Shoes.objects.get(id=id)
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesListEncoder,
            safe=False
        )