from django.db import models

# Create your models here.
class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    import_href = models.URLField()
    

class Shoes(models.Model):
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture = models.URLField(null=True)
    
    bin = models.ForeignKey(
        BinVO,
        related_name='shoes',
        on_delete=models.CASCADE
        )
    
    
    